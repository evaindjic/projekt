# Podatkovni viri

## Podatki primerni za t-test za neodvisna vzorca

1. Količina svinca:                    [opis](svinec.md  ), podatki: [svinec.csv  ](svinec.csv)
2. Rezultati testa geometrije          [opis](geom.md    ), podatki: [geom.csv    ](geom.csv)
3. Število napak                       [opis](premor.md  ), podatki: [premor.csv  ](premor.csv)
3. Število proizvedenih polprevodnikov [opis](industr.md ), podatki: [industr.csv ](industr.csv)
3.  Urna postavka natakarjev           [opis](urpostav.md), podatki: [urpostav.csv](urpostav.csv) 
3.  Življenska doba avtomobilskih gum  [opis](avgume.md  ), podatki: [avgume.csv  ](avgume.csv)

## Linearna regresija

1.  Dolžina ribe               [opis](jezero.md ), podatki: [jezero.csv ](jezero.csv)
2.  Prodajna cena avtomobila   [opis](avcena.md ), podatki: [avcena.csv ](avcena.csv)
3.  Teža možganov pri sesalcih [opis](mozgani.md), podatki: [mozgani.csv](mozgani.csv)
4.  Višina kleka               [opis](klek.md   ), podatki: [klek.csv   ](klek.csv)
5.  Zavorna pot                [opis](zavor.md  ), podatki: [zavor.csv  ](zavor.csv)
6.  Zračni tlak                [opis](forbes.md ), podatki: [forbes.csv ](forbes.csv)

