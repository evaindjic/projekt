# Dolžina ribe

Podatki: [jezero.csv](jezero.csv)

## Opis

Meritve radija lusk in telesne dolžine na vzorcu 38 enoletnih maloustih basov (lat. Micropterus
dolomieu).

## Format

Baza podatkov z 38 meritvami dveh spremenljivk

* *rlusk* je numerična zvezna spremenljivka, ki predstavlja radij lusk (v milimetrih).
* *dolzina* je numerična zvezna spremenljivka, ki predstavlja dolžino telesa (v milimetrih).

## Raziskovalna domneva

Obstaja funkcijska zveza med dolžino in radijem lusk enoletnih maloustih basov.

## Opomba

Konstruirajte linearni regresijski model med transformiranima spremenljivkama `log(dolzina)` in
`log(rlusk)`.